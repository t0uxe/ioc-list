# Afrodita ransomware

## SHA256 hashes

## References
- https://www.vmray.com/analyses/ed58323b71a8/report/overview.html
- https://app.any.run/tasks/07cbb83d-5fa5-4a5d-a39b-6aa6df03b4f2/
- https://id-ransomware.blogspot.com/2020/01/afrodita-ransomware.html
- https://dissectingmalwa.re/not-so-nice-after-all-afrodita-ransomware.html

## Notes
- Mostly distributed UPX packed, no other packer seen so far

## Payloads
```url
riskpartner.hr/wp-content/notnice.jpg
poloprint.hr/wp-content/uploads/2017/05/havefun.png
content-delivery.in/verynice.jpg
```

## Mutex
- 835821AM3218SAZ

## DLL Exports
- Sura
- Ares 0701

## PE Resource section
- IDR_RSA - RSA public key
- Interesting sublang: SUBLANG_ARABIC_OMAN
- Offset 0x000b1070
- Size 0x00000124

## Files dropped
- \AppData\Local\Temp\_uninsep.bat
- client-encrypted-private.key
- client-public.key
- main-public.key
- _README_RECOVERY_.txt

## Embedded RSA key
```
-----BEGIN RSA PUBLIC KEY-----
MIIBIDANBgkqhkiG9w0BAQEFAAOCAQ0AMIIBCAKCAQEAxs2xkeHRygZBupFc2+Z//dLnMbWR/NiXQBmP
10Q7nbG/5jaDcik+eGDh2zz6XYr2Ur+sS1yD4/1XQeIZ/zjcjC43H090nUlELTtq9ED8LqevnrOaMQFy
UIhQU+plY5eJd6KuW2dCdv8n0uBDAzBQRnpjJr0AmnkEzRGD5XCoYtrR061kBAerXQjBxhQSnsMWxE2R
excq38tgf/szXPaoSD1vsSmIwXbc3nTkadYPfjLu6aWWYmikWIi3z+RoUOm7OhmaOu+azPCPBjHc93cB
KsLnxzSHiKRFN4cd0Tu+uvehGl1+v3CK0Zj+nr5OfeNjMGYQj80t0+AqnDQkzwdA/wIBEQ==
-----END RSA PUBLIC KEY-----
```

## Ransom e-mails
- afroditateam@tutanota.com
- afroditasupport@firemail.cc
- afroditasupport@mail2tor.com

## Telegram Support
- @RecoverySupport

## ITW Filenames
- Afrodita.dll
- test.dll
- notnice.png
- havefun.png
- verynice.jpg

## List of extensions to encrypt
```
.WALLET
.CLASS
.INCPAS
.ACCDB
.ACCDR
.ACCDT
.ACCDE
.D3DBSP
.BACKUPDB
.BACKUP
.IBANK
.PKPASS
.MDDATA
.MDBACKUP
.SYNCDB
.LAYOUT
.DAZIP
.ARCH00
.VPP_PC
.MCMETA
.MPQGE
.LITEMOD
.ASSET
.FORGE
.RGSS3A
.WOTREPLAY
.MRWREF
.BLEND
.DESIGN
.YCBCRA
.SQLITEDB
.SQLITE3
.SQLITE
.SAS7BDAT
.PSAFE3
.ERBSQL
.DB-JOURNAL
.MONEYWELL
```

## List of folders to avoid
```
Program Files (x86)
All Users
$GetCurrent
AppData
Program Files
ProgramData
Windows
```

## uninsep.bat cleanup file
```
:Repeat
del "C:\Users\admin\Desktop\notnice.jpg.exe"
if exist "C:\Users\admin\Desktop\notnice.jpg.exe" goto Repeat
rmdir "C:\Users\admin\Desktop"
del "C:\Users\admin\AppData\Local\Temp\_uninsep.bat"
```

## PDB
```
F:\Work\x_Projects\Afrodita - VS2019\Afrodita\cryptopp\rijndael_simd.cpp
F:\Work\x_Projects\Afrodita - VS2019\Afrodita\cryptopp\sha_simd.cpp
F:\Work\x_Projects\Afrodita - VS2019\Afrodita\cryptopp\sse_simd.cpp
```

## Ransom note's filename
```
__README_RECOVERY_.txt
```

## Ransom note
```
~~~ Greetings ~~~
[+] What has happened? [+]
Your files are encrypted, and currently unavailable. You are free to check.
Every file is recoverable by following our instructions below.
Encryption algorithms used: AES256(CBC) + RSA2048 (military/government grade).
[+] Guarantees? [+]
This is our daily job. We are not here to lie to you - as you are 1 of 10000's.
Our only interest is in us getting payed and you getting your files back.
If we were not able to decrypt the data, other people in same situation as you
wouldn't trust us and that would be bad for our buissness --
So it's not in our interest.
To prove our ability to decrypt your data you have 1 file free decryption.
If you don't want to pay the fee for bringing files back that's okey,
but remeber that you will lose a lot of time - and time is money.
Don't waste your time and money trying to recover files using some file
recovery "experts", we have your private key - only we can get the files back.
With our service you can go back to original state in less then 30 minutes.
[+] Service [+]
If you decided to use our service please follow instructions below.
Contact us:
email address: afroditasupport@mail2tor.com, put in cc: afroditasupport@firemail.cc
```
