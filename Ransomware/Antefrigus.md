# Antefrigus ransomware

## SHA256 hashes
- 3cb061bd1c9326ec12d3b5f540d425730245472b72e5295f52b53b82ea03cb68
- b90683251727a6e1e4e846adf7fa29a8dbfba0874cfedcd8a798239130d6c058

## References
- https://www.bleepingcomputer.com/news/security/strange-antefrigus-ransomware-only-targets-specific-drives/
- https://id-ransomware.blogspot.com/2019/11/antefrigus-ransomware.html
- https://twitter.com/malwrhunterteam/status/1195335031633432577
- https://twitter.com/Amigo_A_/status/1195051403888267264
- https://twitter.com/GrujaRS/status/1194692806083796993
- https://twitter.com/VK_Intel/status/1216792575807541249

## Notes
- Demanding a ransom of `1995 USD` in BTC, which doubles when it is not paid within four days and five hours, adding up `3990 USD` in BTC
- Encrypts D:, E:, F:, G:, H:, and I: drives, but interestingly not C:
- Drops a file to `C:\qweasd\test.txt`, in one case with a content: `5823142135788270`
- Check in to `http://iplogger.org/10UJ73`
- Appends a random character extension to encrypted files
- The ransom note: `[extension]-readme.txt`, placed in `C:\Instraction` folder and on the `Desktop`
- Deletes shadow copies via `wmic.exe shadowcopy delete`
- Creates DirectXII.dll file, in one case with a content: `1823673412070204`
- Creates DirectX1I.dll file, in one case with a content: `oqvfhnqo`

## Extension list
```
dll, adv, ani, big, bat, bin, cab, cmd, com, cpl, cur
deskthemepack, diagcab, diagcfg, diagpkg, drv, exe, hlp
icl, icns, ico, ics, idx, ldf, lnk, mod, mpa, msc, msp
msstyles, msu, nls, nomedia, ocx, prf, rom, rtp, scr, shs
spl, sys, theme, themepack, wpx, lock, key, hta, msi, pck
```

## Folder exclusion list
- C:/windows
- C:/Windows
- C:/intel
- C:/nvidia
- C:/ProgramData
- C:/Program Files
- C:/Program Files (x86)

## Task killist
```
taskkill /F /IM aupis80.exe
taskkill /F /IM sql.exe
taskkill /F /IM oracle.exe
taskkill /F /IM ocssd.exe
taskkill /F /IM dbsnmp.exe
taskkill /F /IM synctime.exe
taskkill /F /IM agntsvc.exe
taskkill /F /IM isqlplussvc.exe
taskkill /F /IM xfssvccon.exe
taskkill /F /IM mydesktopservice.exe
taskkill /F /IM ocautoupds.exe
taskkill /F /IM encsvc.exe
taskkill /F /IM tbirdconfig.exe
taskkill /F /IM mydesktopqos.exe
taskkill /F /IM ocomm.exe
taskkill /F /IM dbeng50.exe
taskkill /F /IM sqbcoreservice.exe
taskkill /F /IM excel.exe
taskkill /F /IM infopath.exe
taskkill /F /IM msaccess.exe
taskkill /F /IM mspub.exe
taskkill /F /IM onenote.exe
taskkill /F /IM outlook.exe
taskkill /F /IM powerpnt.exe
taskkill /F /IM steam.exe
taskkill /F /IM thebat.exe
taskkill /F /IM thunderbird.exe
taskkill /F /IM visio.exe
taskkill /F /IM winword.exe
taskkill /F /IM wordpad.exe
```

## TOR gate
```
yboa7nidpv5jdtumgfm4fmmvju3ccxlleut2xvzgn5uqlbjd5n7p3kid.onion
```

## C2 uri params:
```
add_outstuk.php?name=[NAME]&pcName=[PCNAME]&key=[KEY]&memory=[MEMORY]
```

## Ransom e-mails
- antefrigus@cock.li

## PDB
```
G:\sever\Scan\crypro\rijndael_simd.cpp
G:\sever\Scan\crypro\sha_simd.cpp
G:\sever\Scan\crypro\sse_simd.cpp
```

## Ransom notes
```
 $$$$    $$  $$   $$$$$$   $$$$$    $$$$$$   $$$$$    $$$$$$    $$$$    $$  $$    $$$$
$$  $$   $$$ $$     $$     $$       $$       $$  $$     $$     $$       $$  $$   $$
$$$$$$   $$ $$$     $$     $$$$     $$$$     $$$$$      $$     $$ $$$   $$  $$    $$$$
$$  $$   $$  $$     $$     $$       $$       $$  $$     $$     $$  $$   $$  $$       $$
$$  $$   $$  $$     $$     $$$$$    $$       $$  $$   $$$$$$    $$$$     $$$$     $$$$

[+] Whats Happen ? [+]
Your files are encrypted, and currently unavailable.You can check it : all files on you computer has expansion hssjyh.
By the way, everything is possible to recover(restore), but you need to follow our instructions.Otherwise, you cant return your data(NEVER).
[+] What guarantees ? [+]
Its just a business.We absolutely do not care about youand your deals, except getting benefits.If we do not do our workand liabilities - nobody will not cooperate with us.Its not in our interests.
To check the ability of returning files, You should go to our website.There you can decrypt one file for free.That is our guarantee.
If you will not cooperate with our service - for us, its does not matter.But you will lose your timeand data, cause just we have the private key.In practise - time is much more valuable than money.
[+] How to get access on website ? [+]
You have two ways :
1)[Recommended] Using a TOR browser!
a) Download and install TOR browser from this site: https://torproject.org/ 
b) Open our website :  http://yboa7nidpv5jdtumgfm4fmmvju3ccxlleut2xvzgn5uqlbjd5n7p3kid.onion/?hssjyh
  (If you can’t follow the link or other difficulty write to the technical support email : antefrigus@cock.li) 
 2) If TOR blocked in your country, try to use VPN! For this:
 a) Open any browser (Chrome, Firefox, Opera, IE, Edge) and download and install free VPN programm and download TOR browser from this site https://torproject.org/ 
  b) If you are having difficulty purchase bitcoins, or you doubt in buying decryptor, contact to any data recovery company in your country, they will give you more guarantees and take purchase and decryption procedure on themselves. Almost all such companies heared about us and know that our decryption program work, so they can help you.
 When you open our website, put the following data in the input form:
Key:

Extension name :  
hssjyh
---------------------------------------------------------------------------------------- -
!!!DANGER !!!
DONT try to change files by yourself, DONT use any third party software for restoring your data or antivirus solutions - its may entail damge of the private keyand, as result, The Loss all data.
!!!!!!!!!
ONE MORE TIME : Its in your interests to get your files back.From our side, we(the best specialists) make everything for restoring, but please should not interfere.
!!!!!!!!!
```