# DeathRansom \ Wacatac ransomware

## SHA256 hashes
- 05b762354678004f8654e6da38122e6308adf3998ee956566b8f5d313dc0e029
- 0cf124b2afc3010b72abdc2ad8d4114ff1423cce74776634db4ef6aaa08af915
- 13d263fb19d866bb929f45677a9dcbb683df5e1fa2e1b856fde905629366c5e1
- 1e1fcb1bcc88576318c37409441fd754577b008f4678414b60a25710e10d4251
- 2b9c53b965c3621f1fa20e0ee9854115747047d136529b41872a10a511603df8
- 4bc383a4daff74122b149238302c5892735282fa52cac25c9185347b07a8c94c
- 6247f283d916b1cf0c284f4c31ef659096536fe05b8b9d668edab1e1b9068762
- 66ee3840a9722d3912b73e477d1a11fd0e5468769ba17e5e71873fd519e76def
- a45a75582c4ad564b9726664318f0cccb1000005d573e594b49e95869ef25284
- dc9ff5148e26023cf7b6fb69cd97d6a68f78bb111dbf39039f41ed05e16708e4
- e767706429351c9e639cfecaeb4cdca526889e4001fb0c25a832aec18e6d5e06
- f78a743813ab1d4eee378990f3472628ed61532e899503cc9371423307de3d8b
- fedb4c3b0e080fb86796189ccc77f99b04adb105d322bddd3abfca2d5c5d43c8

## References
- https://id-ransomware.blogspot.com/2019/11/wacatac-ransomware.html
- https://twitter.com/search?q=deathransom&src=typed_query
- https://www.fortinet.com/blog/threat-research/death-ransom-attribution.html

## IoCs and Notes
### Others
- Encrypted extension: `.wctc`
- Ransom note: `read_me.txt`
- Check-in : `https://iplogger.org/1Zqq77`
- Shadow copy deletion: `select * from Win32_ShadowCopy` via `ROOT\cimv2`

### IoCs
- `iplogger[.]org/1Zqq77`
- `bitbucket[.]org/scat01/1/downloads/Wacatac_2019-11-16_14-06.exe`
- `bitbucket[.]org/scat01`
- `scat01.mcdir[.]ru`
- `scat01[.]tk`
- `gameshack[.]ru/scat01.exe`

### Ransom e-mails:
- `death@cumallover.me`
- `death@firemail.cc`
- `deathransom@airmail.cc`

### Ransom address:
- `1J9CG9KtJZVx1dHsVcSu8cxMTbLsqeXM5N`

### Registry keys used for private/public key:
- `HKCU\SOFTWARE\Wacatac`
- `HKCU\SOFTWARE\Wacatac\private`
- `HKCU\SOFTWARE\Wacatac\public`

### Excluded files/folders from encryption:
- `programdata`
- `$recycle.bin`
- `program files`
- `windows`
- `all users`
- `appdata`
- `read_me.txt`
- `autoexec.bat`
- `desktop.ini`
- `autorun.inf`
- `ntuser.dat`
- `iconcache.db`
- `bootsect.bak`
- `boot.ini`
- `ntuser.dat.log`
- `thumbs.db`

## Developer's identities:
- scat01
- nedugov
- nedugov99
- SoftEgorka
- undefined_Nedugov
- Phone: `+7951****311`
- VKontakte: `id154704666`
- E-mail: `vitasa01@yandex.ru`
- Instagram: `pro_huligan_`
- WebMoneyID: `372443071304`

## Ransom note:
```
?????????????????????????
??????DEATHRansom ???????
?????????????????????????
Hello dear friend,
Your files were encrypted!
You have only 12 hours to decrypt it
In case of no answer our team will delete your decryption password
Write back to our e-mail: deathransom@airmail.cc


In your message you have to write:
1. YOU LOCK-ID: %s
2. Time when you have paid 0.1 btc to this bitcoin wallet:
1J9CG9KtJZVx1dHsVcSu8cxMTbLsqeXM5N


After payment our team will decrypt your files immediatly


Free decryption as guarantee:
1. File must be less than 1MB
2. Only .txt or .lnk files, no databases
3. Only 1 files


How to obtain bitcoin:
The easiest way to buy bitcoins is LocalBitcoins site. You have to register, click 'Buy bitcoins', and select the seller by payment 
method and price.
https://localbitcoins.com/buy_bitcoins
Also you can find other places to buy Bitcoins and beginners guide here:
http://www.coindesk.com/information/how-can-i-buy-bitcoins/
```
