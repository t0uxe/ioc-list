## Nemty ransomware

## Hashes
- 1d3f2ba1c701ecf04c288b64d9f2470c6f58744d5284174c1cb8e8b3753f3fae
- a127323192abed93aed53648d03ca84de3b5b006b641033eb46a520b7a3c16fc

## References
- https://www.tesorion.nl/nemty-2-2-and-2-3-analysis-of-their-cryptography-and-a-decryptor-for-some-file-types/
- https://www.tesorion.nl/bug-in-nemty-corrupting-the-encryption-of-large-files/
- https://id-ransomware.blogspot.com/2019/08/nemty-ransomware.html
- https://twitter.com/Damian1338/status/1165584237132767242
- https://twitter.com/VK_Intel/status/1214285648036859904
- https://twitter.com/GrujaRS/status/1214206837500841984
- https://twitter.com/GrujaRS/status/1206560801139707906
- https://twitter.com/VK_Intel/status/1202858798672728064
- https://twitter.com/VK_Intel/status/1207769130130182147
- https://twitter.com/GrujaRS/status/1201245856801992705
- https://twitter.com/demonslay335/status/1188880199674408961
- https://twitter.com/VK_Intel/status/1170785426875064325
- https://twitter.com/VK_Intel/status/1171065977066393600
- https://www.acronis.com/en-us/blog/posts/threat-analysis-nemty-ransomware-and-fake-paypal-site
- https://www.optiv.com/blog/nemty-ransomware-deployed-payment-service-phish
- https://www.fortinet.com/blog/threat-research/nemty-ransomware-early-stage-threat.html

## Versions seen
- Nemty 1.0
- Nemty 1.4
- Nemty 1.6
- Nemty 2.0
- Nemty 2.2
- Nemty 2.3
- Nemty 2.5

## Ransom e-mails
- elzmflqxj@tutanota.de
- helpdesk_nemty@aol.com

## Backup phone number
- 09** *** **05

## Nemty gates
```
nemty.hk
nemty10.hk
nemty.top
nemty.top/public/pay.php
zjoxyw5mkacojk5ptn2iprkivg5clow72mjkyk5ttubzxprjjnwapkad.onion/pay
```

## Error message on C2 gate
```
Fatal error: Uncaught [23000] - SQLSTATE[23000]: Integrity constraint violation: 1048 Column 'rsakey' cannot be null trace: #0 C:\OSPanel\domains\localhost\func\rb.php(882): RedBeanPHP\Driver\RPDO->runQuery('INSERT INTO `bo...', Array) #1 C:\OSPanel\domains\localhost\func\rb.php(919): RedBeanPHP\Driver\RPDO->GetAll('INSERT INTO `bo...', Array) #2 C:\OSPanel\domains\localhost\func\rb.php(3547): RedBeanPHP\Driver\RPDO->GetOne('INSERT INTO `bo...', Array) #3 C:\OSPanel\domains\localhost\func\rb.php(4976): RedBeanPHP\Adapter\DBAdapter->getCell('INSERT INTO `bo...', Array, 0) #4 C:\OSPanel\domains\localhost\func\rb.php(5103): RedBeanPHP\QueryWriter\AQueryWriter->insertRecord('bots', Array, Array) #5 C:\OSPanel\domains\localhost\func\rb.php(7646): RedBeanPHP\QueryWriter\AQueryWriter->updateRecord('bots', Array, 0) #6 C:\OSPanel\domains\localhost\func\rb.php(7233): RedBeanPHP\Repository\Fluid->storeBean(Object(RedBeanPHP\OODBBean)) #7 C:\OSPanel\domains\localhost\func\rb.php(8310): RedBeanPHP\Repository->store(Object(RedBeanPHP\ in C:\OSPanel\domains\localhost\func\rb.php on line 720
```

## Notes
- Developed by the same authors who previously distributed JSWorm
- Uses a `RSA 8192-bit` public key
- Demands around `$1,000` for decrypting the files
- Ransom extension: `.nemty`,  later versions seen a change in the template: `._NEMTY_ <random {7}> _`, for ex.: `._NEMTY_VOv3Zme_`
- Ransom note: `NEMTY-DECRYPT.txt`, later a random value was introduced `NEMTY_VFRLXV9-DECRYPT.txt`
- Has network enumeration capability
- Checks in to `api.db-ip.com/v2/free/[IP]/countryName`
- At the end of every encrypted file, there's a marker of the same random value, which the extension containes
- Internal TOR proxy goes to 127.0.0.1:9050/public/gate?data= (wrong port, 9051)

## Commands seen
```
vssadmin.exe delete shadows / all / quiet
bcdedit / set {default} bootstatuspolicy ignoreallfailures
bcdedit / set {default} recoveryenabled no
wbadmin delete catalog -quiet
wmic shadowcopy delete
cmd.exe / c vssadmin resize shadowstorage / for = C: / on = C: / maxsize = 401MB
cmd.exe / c vssadmin resize shadowstorage / for = C: / on = C: / maxsize = unbounded
```

## Seen mutexes
- hate
- fuckav
- just_a_little_game
- just_a_game

## Blacklisted extensions
```
.cab, .CAB
.cmd, .CMD
.com, .COM
.cpl, .CPL
.dll, .DLL
.exe, .EXE
.ini, .INI
.lnk, .LNK
.log, .LOG
.ttf, .TTF
.url, .URL
.nemty
```

## Skip encrypting these files/folders
```
DECRYPT.txt
$RECYCLE.BIN
rsa
NTDETECT.COM
ntldr
MSDOS.SYS
IO.SYS
boot.ini
AUTOEXEC.BAT
ntuser.dat
desktop.ini
CONFIG.SYS
RECYCLER
BOOTSECT.BAK
bootmgr
programdata
appdata
windows
Microsoft
Common Files
```

## Anti-CIS
- Russia
- Belarus
- Kazakhstan
- Tajikistan
- Ukraine
- Azerbaijan
- Armenia
- Kyrgyzstan
- Moldova

## Stopped services
```
DbxSvc
OracleXETNSListener
OracleServiceXE
AcrSch2Svc
AcronisAgent
Apache2.4
SQLWriter
MSSQL$SQLEXPRESS
MSSQLServerADHelper100
MongoDB
SQLAgent$SQLEXPRESS
SQLBrowser
```

## Process killlist
```
sql
winword
wordpad
outlook
thunderbird
oracle
excel
onenote
virtualboxvm
node
QBW32
WBGX
Teams
Flow
```

## Ransom note
```
---=== NEMTY PROJECT ===---
[+] Whats Happen? [+]
Your files are encrypted, and currently unavailable. You can check it: all files on you computer has extension .nemty
By the way, everything is possible to restore, but you need to follow our instructions. Otherwise, you cant return your data (NEVER).
[+] What guarantees? [+]
It's just a business. We absolutely do not care about you and your deals, except getting benefits.
If we do not do our work and liabilities - nobody will not cooperate with us.
It's not in our interests.
If you will not cooperate with our service - for us, its does not matter. But you will lose your time and data, cause just we have the private key.
In practise - time is much more valuable than money.
[+] How to get access on website? [+]
  1) Download and install TOR browser from this site: https://torproject.org/
  2) Open our website: zjoxyw5mkacojk5ptn2iprkivg5clow72mjkyk5ttubzxprjjnwapkad.onion/pay
When you open our website, follow the instructions and you will get your files back.
Configuration file path: C:\Users\admin
```