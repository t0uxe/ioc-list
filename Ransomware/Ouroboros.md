# Ouroboros v7 ransomware

## References
- https://app.any.run/tasks/d8f2f343-cc63-40ca-8571-2d5c8d139f16

## Ransom e-mail
- Honeylock@protonmail.com

## Ransom extension
- .odveta

## Renamed files format
- .Email=[...]ID=[...]

## PDB
- C:\Users\LEGION\Desktop\New folder\sha_simd.cpp
- C:\Users\LEGION\Desktop\New folder\gcm_simd.cpp
- C:\Users\LEGION\Desktop\New folder\sse_simd.cpp
- D:\Ouroboros v7\Ouroborosv7\Release\Ouroborosv7.pdb

## C2 check-in
```
sfml-dev.org/ip-provider.php
User-Agent: libsfml-network/2.x
```

## C2 server
```
80.82.69.52 - WIN-CMHPELDRE6E
```

## Uri parameters
- &mail=
- &id=
- &key=
- &disk=
- &ip=

## Commands executed
- bcdedit /set {default} bootstatuspolicy ignoreallfailures
- bcdedit /set {default} recoveryenabled no
- netsh advfirewall set  currentprofile state off
- netsh firewall set opmode mode=disable
- vssadmin delete shadows /all
- wbadmin delete catalog -quiet

## Dropped files
- C:\ProgramData\id.txt
- C:\ProgramData\ids.txt
- C:\ProgramData\Pkey.txt
- C:\ProgramData\info.txt
- C:\ProgramData\uiapp.exe

## Extension list
```
.exe
.mdf
.pst
.bak
.tib
.DBF
.zip
```

## Process killlist
```
sqlserver.exe
msftesql.exe
sqlagent.exe
sqlbrowser.exe
sqlwriter.exe
mysqld.exe
mysqld-nt.exe
mysqld-opt.exe
```

## Service stoplist
```
net stop SQLWriter
net stop SQLBrowser
net stop MSSQLSERVER
net stop MSSQL$CONTOSO1
net stop MSDTC
net stop SQLSERVERAGENT
net stop vds
```

## RSA key
```
MIIBIDANBgkqhkiG9w0BAQEFAAOCAQ0AMIIBCAKCAQEAzA+0svdK/zcghzPRS7F+pPzCjAlygcNhQj/T5g7HigLegVH/Fi2dhjBDxjt9Ly3aPsHpTb32ub2xb3gvv2VcBi4P
VOClceG9Pq6M6HlUGYt1yhSRx8v+0CHX9Dg9rA/3SMhlqsVodtHtMbefPnPhto1/QY4FCcS3xGcC97Ja+oDkCVnkES8U1xeHno5kIlIWMulS5pjM6D3hss5yjUjIHiSr06QS
4gCLX1ZijMo0wA3y6k9RUm9Me8MYiss/39RzsgGwDv5+DNxkLgSU25Sa2NZ8iG+Vufk/CkWe9CQj/SRCHm/mVQpNlfbthTTGh3OXy36pu46nYv3fS/ulkqHTaQIBEQ==
```

## Ransom note:
```
in Case of no answer contact : Honeylock@cock.li
You Can Learn How to Buy Bitcoin From This links Below
https://localbitcoins.com/buy_bitcoins
https://www.coindesk.com/information/how-can-i-buy-bitcoins
Your Id: 
***All Your Files Has Been Encrypted***
You have To Pay To Get Decryption Tool with Key
The Time That Your System infected has been logged in Our Server
So Being late more Than 48hours To contact or paying us will Double The price
And using 3rd party applications may damage your files and increase the Price
You Can Send some Files that not Contains Valuable Data To make Sure That Your Files Can be Back with our Tool
The Payment Should Be with Cryptocurrencies Like Bitcoin(BTC) Send Email to Know the Price And Do an Agreement
Our Email: 
```