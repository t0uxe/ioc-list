# PureLocker ransomware
Discovered by IBM IRIS X-Force and IntezerLabs  

## SHA256 hashes
`1fd15c358e2df47f5dde9ca2102c30d5e26d202642169d3b2491b89c9acc0788`  
`300c58478a93e4160dbbd01598d2f1df3f51519687d2c954682ecb7813386ab4`  
`c592c52381d43a6604b3fc657c5dc6bee61aa288bfa37e8fc54140841267338d`  

## References
https://exchange.xforce.ibmcloud.com/collection/99c7156cff70e1d8e1687ab7dadc8c0e  
https://www.intezer.com/blog-purelocker-ransomware-being-used-in-targeted-attacks-against-servers/  

## Notes
- Written in PureBasic
- Execute with regsvr32 /s /i
- Mutex: 04780006780E6407
- XOR key: D3F3CEBB972965
- Decoded string: CR1
- Encrypted extension: .CR1
- Claims to encrypt with: AES-256-CBC + RSA-4096
- Encryption: 32-byte random AES key and 16-byte random IV
- Logfile: dbg.txt
- Ransom note: YOUR_FILES.txt
- Shadow copy adjustment: wmic shadowstorage SET MaxSpace=337000000
- Ransom e-mail: cr1-silvergold1@protonmail.com

## Yara rules
Some of these rules are solely VT retro,- livehunt rules and not suitable for deploying in production networks.

```yara
// VT livehunt rule - Detect new PureLocker CARO sigs
rule purelocker_carosigs
{
  meta:
    author = "Albert Zsigovits"
    reference = "c592c52381d43a6604b3fc657c5dc6bee61aa288bfa37e8fc54140841267338d"
    family = "PureLocker ransomware"
    
  condition:
    new_file
      and
      (
    (signatures matches /.*PureLocker.*/)
      or
     (signatures matches /.*PURELOCKER.*/)
      )
}

// VT livehunt rule - Detect new PureLocker based on the previously seen imphashes
rule purelocker_imphash
{
  meta:
    author = "Albert Zsigovits"
    reference = "c592c52381d43a6604b3fc657c5dc6bee61aa288bfa37e8fc54140841267338d"
    family = "PureLocker ransomware"
    
  condition:
    new_file
      and
      (
    (imphash contains "cc2d1da2e5791504e1bf336fd23d0a28")
      or
    (imphash contains "59e0431c441419a7ff332859b546442e")
      or
    (imphash contains "e0b78fab94dec81f75eba07d46605381")
      )
}

// c592c52381d43a6604b3fc657c5dc6bee61aa288bfa37e8fc54140841267338d
// VT livehunt rule - Timestomped - 2001-08-17 20:52:32
// FP prone
import "pe"
rule purelocker_timestamp
{
  meta:
    author = "Albert Zsigovits"
    reference = "c592c52381d43a6604b3fc657c5dc6bee61aa288bfa37e8fc54140841267338d"
    family = "PureLocker ransomware"    
    
  condition:
    new_file
      and
    filesize < 200KB
      and
    pe.timestamp == 998081552
}

// This unique resource section matches in both seen samples, but might produce FPs
import "pe"
rule purelocker_sections
{
  meta:
    author = "Albert Zsigovits"
    reference = "c592c52381d43a6604b3fc657c5dc6bee61aa288bfa37e8fc54140841267338d"
    family = "PureLocker ransomware"    
    
  condition:
    pe.number_of_sections == 6
      and
    pe.sections[4].name == ".rsrc"
      and
    pe.sections[4].virtual_size == 1068
      and
    pe.sections[4].raw_data_size == 1536
}

// VT livehunt rule - Match on previously seen filenames, might produce FPs
rule purelocker_filename
{
  meta:
    author = "Albert Zsigovits"
    reference = "c592c52381d43a6604b3fc657c5dc6bee61aa288bfa37e8fc54140841267338d"
    family = "PureLocker ransomware"    
    
  condition:
    new_file
      and
    filesize < 200KB
      and
      (
    (file_name contains "cryptopp")
      or
    (file_name contains "cryptopp.dll")
      or
    (file_name contains "cryptopp_w2.dll")
      )
}

import "pe"
rule purelocker_exports
{
  meta:
    author = "Albert Zsigovits"
    reference = "c592c52381d43a6604b3fc657c5dc6bee61aa288bfa37e8fc54140841267338d"
    family = "PureLocker ransomware"    
    
  condition:
    new_file
      and
    pe.exports("DeleteMusic")
      and
    pe.exports("FindMusic")
      and
    pe.exports("MoveMusic")
      and
    pe.exports("SeekMusic")
      and
    pe.exports("UploadMusic")
      and
    pe.exports("DllRegisterServer")
}

rule purelocker_pe
{
  meta:
    author = "Albert Zsigovits"
    reference = "c592c52381d43a6604b3fc657c5dc6bee61aa288bfa37e8fc54140841267338d"
    family = "PureLocker ransomware"    
    
  strings:
    $file = "cryptopp.dll"
    $import0 = "DeleteMusic"
    $import1 = "FindMusic"
    $import2 = "MoveMusic"
    $import3 = "SeekMusic"
    $import4 = "UploadMusic"
    $import5 = "DllRegisterServer"

  condition:
    uint16(0) == 0x5a4d and filesize < 200KB and all of them
}

rule purelocker_inmemory_note
{
  meta:
    author = "Albert Zsigovits"
    reference = "c592c52381d43a6604b3fc657c5dc6bee61aa288bfa37e8fc54140841267338d"
    family = "PureLocker ransomware"    
    
  strings:
    $note0 = "To decrypt your files contact us at: cr1-silvergold1@protonmail.com" wide
    $note1 = "Shadows copies were removed, original files were overwritten, renamed and deleted using safe methods." wide
    $note2 = ", after that the recovery of your files will not be possible." wide
    $note3 = "Your private key will be deleted after 7 days starting from: " wide
    $note4 = "All your files have been encrypted using: AES-256-CBC + RSA-4096." wide
    $note5 = "Recovery is not possible without own RSA-4096 private key." wide
    $note6 = "Only we can decrypt your files!" wide

  condition:
    4 of them
}

rule purelocker_inmemory
{
  meta:
    author = "Albert Zsigovits"
    reference = "c592c52381d43a6604b3fc657c5dc6bee61aa288bfa37e8fc54140841267338d"
    family = "PureLocker ransomware"    
    
  strings:
    $pure0 = "dbg.txt" wide
    $pure1 = "YOUR_FILES.txt" wide
    $pure2 = "CR1" wide
    $pure3 = "/s /i" wide
    $pure4 = "/V /C set " wide
    $pure5 = "& <NUL set /p " wide
    $pure6 = "& del /F !_" wide
    $pure7 = "&& (For /L %x in (1, 1, 7999) do set " wide
    $pure8 = ") && <NUL set /p " wide
    $pure9 = "wmic shadowstorage SET MaxSpace=337000000" wide
    $pure10 = ".CR1" wide
    $pure11 = "advapi32.dll" wide
    $pure12 = "regsvr32.exe" wide
    $pure13 = "ntdll.dll" wide
    $pure14 = "Global\\" wide
    $pure15 = "\\KnownDlls" wide

  condition:
    8 of them
}
```

## Ransom note
```
#CR1
All your files have been encrypted using: AES-256-CBC + RSA-4096.
Shadows copies were removed, original files were overwritten, renamed and deleted using safe methods.
Recovery is not possible without own RSA-4096 private key.
Only we can decrypt your files!
To decrypt your files contact us at: cr1-silvergold1@protonmail.com 
Your private key will be deleted after 7 days starting from: 15/10/2019, after that the recovery of your files will not be possible.
```
