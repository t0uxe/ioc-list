## SNAKE / EKANS ransomware

## Hashes
- e5262db186c97bbe533f0a674b08ecdafa3798ea7bc17c705df526419c168b60

## References
- https://www.bleepingcomputer.com/news/security/snake-ransomware-is-the-next-threat-targeting-business-networks/
- https://twitter.com/VK_Intel/status/1214333066245812224
- https://github.com/sysopfb/open_mal_analysis_notes/blob/master/e5262db186c97bbe533f0a674b08ecdafa3798ea7bc17c705df526419c168b60.md

## Sandbox runs
- https://app.any.run/tasks/040f5530-fe29-42d6-b312-e3d338449f51
- https://www.vmray.com/analyses/e5262db186c9/report/overview.html
- https://hybrid-analysis.com/sample/e5262db186c97bbe533f0a674b08ecdafa3798ea7bc17c705df526419c168b60?environmentId=100
- https://analyze.intezer.com/#/files/e5262db186c97bbe533f0a674b08ecdafa3798ea7bc17c705df526419c168b60/sub/fb9e8d93-92a9-477f-87f5-24f5c7a653d5

## Notes
- Ransom note: `Fix-Your-Files.txt`
- Ransom note put into: `C:\Users\Public\Desktop`
- Ransom e-mail: `bapcocrypt@ctemplar.com`
- Go build ID: `"X6lNEpDhc_qgQl56x4du/fgVJOqLlPCCIekQhFnHL/rkxe6tXCg56Ez88otHrz/Y-lXW-OhiIbzg3-ioGRz"`
- Filemarker: `EKANS`
- Mutex: `Global\EKANS`
- Deletes shadow copies via: `SELECT * FROM Win32_ShadowCopy`
- It appends a random 5 char string to the encrypted files' extension. `secret.pdf` becomes `secret.pdfbNcKl`

## RSA key:
```
-----BEGIN RSA PUBLIC KEY-----
MIIBCgKCAQEAyQ+M5ve829umuy9+BSsUX/krgdF83L3m8/uxRvKX5EZbSh1+buON
ZYr5MjfhrdiOGnrbB1j0Fy31U/uzvWcy7VvK/zcsO/5aAhujhHB/qMAVpZ8zT5BB
ujT1Bvsith/BXgtM99MixD8oZ67VDZaRM9TPE89WuAjnaBZORrk48wFcn1DOAAHD
Z9z9komtqIH1fm3Y0Q6P76nUscLsYOme082L217Th/lTMoqqs4cF2rn9O9Vp4V9U
aCs4XVxGSpcuqbIscfpf0cm44P2eOEk+sbZdahO9C6fezt7YF4OCJ4Vz3qqMD6z4
+6d7FRxUu6k3Te2T2bWBZnsDO30pYFi/gwIDAQAB
-----END RSA PUBLIC KEY-----
```

## Sample encrypted file
- FileName
- IV
- ENCRYPTED_AES_Key

```byte
a(`..g.ú.w.û~¸¶.
Ùä7U.F..Lÿ.....n
odceikemblegmpmk
clo.ÿ......FileN
ame.....IV.....E
NCRYPTED_AES_Key
.....þ.*ÿ...C:\a
utoexec.bat
```

## Skips encrypting the following locations
- windir
- SystemDrive
- :\$Recycle.Bin
- :\ProgramData
- :\Users\All Users
- :\Program Files
- :\Local Settings
- :\Boot
- :\System Volume Information
- :\Recovery
- \AppData\

## Ransom note
```text
--------------------------------------------
| What happened to your files? 
--------------------------------------------

We breached your corporate network and encrypted the data on your computers. The encrypted data includes documents, databases, photos and more - all were encrypted using a military grade encryption algorithms (AES-256 and RSA-2048). You cannot access those files right now. But dont worry!

You can still get those files back and be up and running again in no time. 

---------------------------------------------
| How to contact us to get your files back?
---------------------------------------------

The only way to restore your files is by purchasing a decryption tool loaded with a private key we created specifically for your network. 

Once run on an effected computer, the tool will decrypt all encrypted files - and you can resume day-to-day operations, preferably with better cyber security in mind. If you are interested in purchasing the decryption tool contact us at bapcocrypt@ctemplar.com

-------------------------------------------------------
| How can you be certain we have the decryption tool?
-------------------------------------------------------

In your mail to us attach up to 3 files (up to 3MB, no databases or spreadsheets).

We will send them back to you decrypted.
```

## Blacklisted extensions and files:
```
.docx
.dll
.exe
.sys
.mui
.tmp
.lnk
.config
.manifest
.tlb
.olb
.blf
.ico
.regtrans-ms
.devicemetadata-ms
.settingcontent-ms
.bat
.cmd
.ps1
desktop.ini
iconcache.db
ntuser.dat
ntuser.ini
ntuser.dat.log1
ntuser.dat.log2
usrclass.dat
usrclass.dat.log1
usrclass.dat.log2
bootmgr
bootnxt
ntldr
NTDETECT.COM
boot.ini
bootfont.bin
bootsect.bak
desktop.ini
ctfmon.exe
iconcache.db
ntuser.dat
ntuser.dat.log
ntuser.ini
thumbs.db
```

## Taskkill list:
```
ccflic0.exe
ccflic4.exe
healthservice.exe
ilicensesvc.exe
nimbus.exe
prlicensemgr.exe
certificateprovider.exe
proficypublisherservice.exe
proficysts.exe
erlsrv.exe
vmtoolsd.exe
managementagenthost.exe
vgauthservice.exe
epmd.exe
hasplmv.exe
spooler.exe
hdb.exe
ntservices.exe
n.exe
monitoringhost.exe
win32sysinfo.exe
inet_gethost.exe
taskhostw.exe
proficy administrator.exe
ntevl.exe
prproficymgr.exe
prrds.exe
prrouter.exe
prconfigmgr.exe
prgateway.exe
premailengine.exe
pralarmmgr.exe
prftpengine.exe
prcalculationmgr.exe
prprintserver.exe
prdatabasemgr.exe
preventmgr.exe
prreader.exe
prwriter.exe
prsummarymgr.exe
prstubber.exe
prschedulemgr.exe
cdm.exe
musnotificationux.exe
npmdagent.exe
client64.exe
keysvc.exe
server_eventlog.exe
proficyserver.exe
server_runtime.exe
config_api_service.exe
fnplicensingservice.exe
workflowresttest.exe
proficyclient.exe
vmacthlp.exe
msdtssrvr.exe
sqlservr.exe
msmdsrv.exe
reportingservicesservice.exe
dsmcsvc.exe
winvnc4.exe
client.exe
collwrap.exe
bluestripecollector.exe
```