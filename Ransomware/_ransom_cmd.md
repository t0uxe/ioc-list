## Ransom commands
Most common OS commands executed by ransomware

## Bcdedit
- bcdedit /set {default} bootstatuspolicy ignoreallfailures
- bcdedit /set {default} recoveryenabled no
- bcdedit /set {current} safeboot minimal

## Netsh
- netsh advfirewall set currentprofile state off
- netsh firewall set opmode disable
- netsh firewall set opmode mode=disable

## Shutdown
- shutdown.exe /r /f /t 00

## Vssadmin
- vssadmin delete shadows /all /quiet
- vssadmin resize shadowstorage /for=C: /on=C: /maxsize=401MB
- vssadmin resize shadowstorage /for=C: /on=C: /maxsize=unbounded

## Wbadmin
- wbadmin delete backup
- wbadmin delete catalog -quiet
- wbadmin delete systemstatebackup
- wbadmin delete systemstatebackup -deleteOldest
- wbadmin delete systemstatebackup -keepversions:0

## Wevtutil
- wevtutil cl system
- wevtutil cl security
- wevtutil cl application

## Wmic
- wmic shadowcopy /nointeractive
- wmic shadowcopy delete